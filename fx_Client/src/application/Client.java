package application;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import javafx.concurrent.Task;

public class Client {

	private Socket serverSocket;
	private ObjectOutputStream out;

	Task<String> task;

	public void initClient() {
		try {
			serverSocket = new Socket("localhost", 1111);
			out = new ObjectOutputStream(serverSocket.getOutputStream());

			task = new Task<>() {

				@Override
				protected String call() throws Exception {
					ObjectInputStream in = new ObjectInputStream(serverSocket.getInputStream());

					Object obj = null;
					while ((obj = in.readObject()) != null) {
						updateValue("Client msg: " + obj);
					}

					return null;
				}
			};

			new Thread(task).start();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Task<String> getTask() {
		return task;
	}

	public void setTask(Task<String> task) {
		this.task = task;
	}

	public void sendMessage(String msg) {

		try {
			out.writeObject("hallo Server");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
