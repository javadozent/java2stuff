package application;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class ClientController {
	
	private Client client;


    @FXML
    private TextField inTextField;

    @FXML
    void sendAction(ActionEvent event) {
    	client.sendMessage(inTextField.getText());
    }

    @FXML
    void initialize() {

    	client = new Client();
    	client.initClient();
    	
    	client.getTask().valueProperty().addListener( (a,b,c)->{
    		System.out.println("Controller Client: "+c);
    	});
    }
}
