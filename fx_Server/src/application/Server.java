package application;
	
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {
	
	
	private ArrayList<ClientHandler> clientList = new ArrayList<>();
	int count =0;

	public Server() {
		try (ServerSocket serverSocket = new ServerSocket(1111)) {
			System.out.println("Server startet..." + serverSocket.getLocalSocketAddress());

			while (true) {
				Socket clientSocket = serverSocket.accept();// 
				
				System.out.println("Server accept...");
				
				serverHandler(clientSocket);
				
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void serverHandler(Socket clientSocket)  {
		
		new Thread(
				()->{
					try {
						
						ObjectOutputStream out = new ObjectOutputStream(clientSocket.getOutputStream());
						ObjectInputStream in = new ObjectInputStream(clientSocket.getInputStream());
					
						clientList.add(new ClientHandler(clientSocket, in, out));
						
						for (ClientHandler clientHandler : clientList) {
							count++;
							Object obj=null;
							while ((obj = in.readObject()) != null) {
								String s = String.valueOf(clientHandler.getIn().readObject());
								
								clientHandler.getOut().writeObject(s.toUpperCase()+": "+count);
								
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				).start();
	}

	public static void main(String[] args) {
		new Server();

	}

}
