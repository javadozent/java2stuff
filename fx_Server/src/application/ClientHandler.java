package application;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ClientHandler {

	private Socket toClientSocket;
	private ObjectInputStream in;
	private ObjectOutputStream out;
	public ClientHandler(Socket toClientSocket, ObjectInputStream in, ObjectOutputStream out) {
		super();
		this.toClientSocket = toClientSocket;
		this.in = in;
		this.out = out;
	}
	public Socket getToClientSocket() {
		return toClientSocket;
	}
	public void setToClientSocket(Socket toClientSocket) {
		this.toClientSocket = toClientSocket;
	}
	public ObjectInputStream getIn() {
		return in;
	}
	public void setIn(ObjectInputStream in) {
		this.in = in;
	}
	public ObjectOutputStream getOut() {
		return out;
	}
	public void setOut(ObjectOutputStream out) {
		this.out = out;
	}
	
	

}
